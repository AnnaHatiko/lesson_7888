angular
  .module('UserApp')
  .factory('UsersService', function ($resource, $http) {
    return $resource('https://jsonplaceholder.typicode.com/users/:userId/:postsController', {
      userId: '@userId',
      postsController: '@postsController'
    }, {
      update: {
        method: 'PUT'
      },
        getPosts: {
            params:{
                postsController: 'posts'
            },
            isArray: true
        }
    })
  })
//